"show line number
set nu 

"high light search
set hlsearch


"color theme
colorscheme murphy


"plugin on
filetype plugin on
set nocompatible




""""""binding keys
"""buffer list
nnoremap <silent> gb :bprevious<CR>
nnoremap <silent> gB :bnext<CR>
nnoremap <silent> Gb :bfirst<CR>
nnoremap <silent> GB :blast<CR>


"""explore dir of current active file
cnoremap <expr> %% getcmdtype()==':'?expand('%:h').'/':'%%'



"""NERDTree plugin
map <F7> :NERDTreeToggle<CR>  
imap <F7> <ESC>:NERDTreeToggle<CR>
